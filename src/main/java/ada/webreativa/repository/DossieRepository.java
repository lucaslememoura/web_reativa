package ada.webreativa.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import ada.webreativa.domain.entity.Dossie;
import ada.webreativa.utils.Espera;

@Repository
public class DossieRepository {
	
	public static Map<String, Dossie> dossies;
	
	public DossieRepository() {
		dossies = new HashMap<>();
	}
	
	public void save(Dossie dossie) {
		Espera.esperar(200);
		dossies.put(dossie.getIdDossie(), dossie);
	}
	
	public Dossie findById(String id) {
		return dossies.get(id);
	}
}
