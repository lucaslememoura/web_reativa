package ada.webreativa.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import ada.webreativa.domain.entity.Minuta;
import ada.webreativa.utils.Espera;

@Repository
public class MinutaRepository {
	
	public static Map<String, Minuta> minutas;
	
	public MinutaRepository() {
		minutas = new HashMap<>();
	}
	
	public void save(Minuta minuta) {
		Espera.esperar(200);
		minutas.put(minuta.getIdInterno(), minuta);
	}
	
	public Minuta findById(String id) {
		return minutas.get(id);
	}
}
