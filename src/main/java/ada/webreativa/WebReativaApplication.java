package ada.webreativa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class WebReativaApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebReativaApplication.class, args);
	}
}
