package ada.webreativa.service;

import org.springframework.stereotype.Component;

import ada.webreativa.domain.entity.Dossie;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;


@Component
public class MessageriaPublisher {
    private Sinks.Many<Dossie> messageriaSinks;

    public MessageriaPublisher(){ this.messageriaSinks =  Sinks.many().multicast().onBackpressureBuffer(); }

    public void publish(Dossie dossie){
        this.messageriaSinks.tryEmitNext(dossie);
    }

    public Flux<Dossie> getNewsFlux() {
        return this.messageriaSinks.asFlux();
    }
}
