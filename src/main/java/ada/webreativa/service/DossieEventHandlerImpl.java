package ada.webreativa.service;

import ada.webreativa.domain.entity.Dossie;
import ada.webreativa.repository.DossieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.scheduler.Schedulers;


@Service
public class DossieEventHandlerImpl implements DossieEventHandler {

    @Autowired DossiePublisher dossiePublisher;


    @Autowired
    private DossieRepository dossieRepository;

    @Override
    public void sendEvent(Dossie dossie) {
        dossiePublisher.publish(dossie);
    }


    public DossieEventHandlerImpl(MessageriaPublisher dossiePublisher){
        dossiePublisher.getNewsFlux().subscribeOn(Schedulers.newSingle("new thred")).subscribe(
                dossie -> {
                    dossie.fechar();
                    dossieRepository.save(dossie);
                },
                error -> System.out.println("error " + error)
        );
    }
}
