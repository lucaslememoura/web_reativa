package ada.webreativa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ada.webreativa.domain.entity.Dossie;
import ada.webreativa.domain.entity.Minuta;
import ada.webreativa.repository.DossieRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DossieService {

	@Autowired
	private MinutaService minutaService;
	
	@Autowired
	private DossieRepository dossieRepository;
	
	@Autowired
	private DigitalFolderService digitalFolderService;

	@Autowired private DossieEventHandler dossieEventHandler;

	@Autowired private
	MessageriaEventHandler messageriaEventHandler;
	
	public Dossie criarDossie(String idCliente, String nomeCliente, String emailCliente) {
		Dossie dossie = new Dossie(idCliente, nomeCliente, emailCliente);
		dossieRepository.save(dossie);
		log.info("Criou dossie: {} ", dossie);


		for (int i = 1; i < 3; i++) {
			Minuta minuta = minutaService.gerarMinuta("Minuta "+i);
			dossie.addMinuta(minuta);
			digitalFolderService.enviarMinutaParaPastaCliente(dossie.getIdCliente(), minuta);
			minutaService.atualizarMinuta(minuta);

		}

		messageriaEventHandler.sendEvent(dossie);


//		dossie.fechar();
//		dossieRepository.save(dossie);

		dossieEventHandler.sendEvent(dossie);
//		emailService.enviarEmailDossie(dossie);
		log.info("Encerrou dossie: {} ", dossie);
		return dossie;

	}
	
}
