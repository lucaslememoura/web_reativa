package ada.webreativa.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ada.webreativa.domain.entity.Minuta;
import ada.webreativa.repository.MinutaRepository;
import ada.webreativa.utils.Espera;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MinutaService {
	
	@Autowired
	private MinutaRepository minutaRepository;
	
	public Minuta gerarMinuta(String nome) {
		Minuta minuta = new Minuta();
		minuta.setNome(nome);
		minuta.setIdInterno(UUID.randomUUID().toString());
		minutaRepository.save(minuta);

		Espera.esperar(500);
		log.info("Criou minuta: {}", minuta);
		return minuta;
	}
	
	public void atualizarMinuta(Minuta minuta) {
		minutaRepository.save(minuta);
		Espera.esperar(500);
		log.info("Atualizou minuta: {}", minuta);
	}
}
