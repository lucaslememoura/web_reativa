package ada.webreativa.service;

import org.springframework.stereotype.Component;

import ada.webreativa.domain.entity.Dossie;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;


@Component
public class DossiePublisher {
    private Sinks.Many<Dossie> dossieSinks;

    public DossiePublisher(){ this.dossieSinks =  Sinks.many().multicast().onBackpressureBuffer(); }

    public void publish(Dossie dossie){
        this.dossieSinks.tryEmitNext(dossie);
    }

    public Flux<Dossie> getNewsFlux() {
        return this.dossieSinks.asFlux();
    }
}
