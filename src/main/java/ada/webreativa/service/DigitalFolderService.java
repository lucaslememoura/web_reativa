package ada.webreativa.service;

import java.util.UUID;

import org.springframework.stereotype.Service;

import ada.webreativa.domain.entity.Minuta;
import ada.webreativa.utils.Espera;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DigitalFolderService {

	public void enviarMinutaParaPastaCliente(String idCliente, Minuta minuta) {
		minuta.setIdExterno(UUID.randomUUID().toString());
		log.info("Enviou minuta para a pasta do cliente {}: {}", idCliente, minuta);
		Espera.esperar(500);
	}
	
}
