package ada.webreativa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ada.webreativa.domain.entity.Dossie;
import ada.webreativa.gateway.EmailGateway;
import reactor.core.scheduler.Schedulers;


@Service
public class MessageriaEventHandlerImpl implements MessageriaEventHandler {

    @Autowired MessageriaPublisher messageriaPublisher;

    @Autowired
    EmailGateway emailGateway;

    @Override
    public void sendEvent(Dossie dossie) {
    	System.out.println("Enviando dossie para messageriaPublisher");
        messageriaPublisher.publish(dossie);
    	System.out.println("Enviou email via messageriaPublisher");
    }


    public MessageriaEventHandlerImpl(MessageriaPublisher dossiePublisher){
        dossiePublisher.getNewsFlux().subscribeOn(Schedulers.newSingle("new thred")).subscribe(
                dossie -> {
                	System.out.println("Enviando dossie via emailGateway");	
                	emailGateway.enviarEmail(dossie);
                	System.out.println("Enviou dossie via emailGateway");
                },
                error -> System.out.println("error " + error)
        );
    }
    
    
}
