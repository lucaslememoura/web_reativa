package ada.webreativa.service;

import org.springframework.stereotype.Service;

import ada.webreativa.domain.entity.Dossie;
import ada.webreativa.domain.entity.Minuta;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailService {

	public static final String ASSUNTO_EMAIL = "Documentos";
	public static final String CORPO_EMAIL = "%s,\nseguem os documentos referentes à sua solicitação:";
	public static final String CORPO_MINUTA = "\n%s: http://servidor.com.br/pastadigital/%s";
			
			
	public void enviarEmailDossie(Dossie dossie) {
		StringBuilder corpo = new StringBuilder(String.format(CORPO_EMAIL, dossie.getNomeCliente()));
		for (Minuta minuta : dossie.getMinutas()) {
			corpo.append(String.format(CORPO_MINUTA, minuta.getNome(), minuta.getIdExterno()));
		}
		log.info("Enviando email:\nDestinatário: {}\nAssunto: {}\n\n{}", dossie.getEmailCliente(), ASSUNTO_EMAIL, corpo.toString());
	}
}
