package ada.webreativa.service;

import ada.webreativa.domain.entity.Dossie;
import org.springframework.stereotype.Component;

@Component
public interface MessageriaEventHandler {
    void sendEvent(Dossie dossie);
}
