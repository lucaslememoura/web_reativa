package ada.webreativa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ada.webreativa.domain.entity.Dossie;
import ada.webreativa.service.DossieService;

@RestController
@RequestMapping("/dossie")
public class DossieController {
	
	@Autowired
	private DossieService dossieService;
	
	@PostMapping
    public ResponseEntity<Dossie> criar(@RequestBody Dossie dossie) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dossieService.criarDossie(dossie.getIdCliente(), dossie.getNomeCliente(), dossie.getEmailCliente()));
    }
}
