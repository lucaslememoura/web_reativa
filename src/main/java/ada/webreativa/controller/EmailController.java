package ada.webreativa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ada.webreativa.domain.entity.Dossie;
import ada.webreativa.service.EmailService;

@RestController
@RequestMapping("/email")
public class EmailController {
	
	@Autowired
	private EmailService emailService;
	
	private static boolean erro = false;
	
	@PostMapping
    public ResponseEntity<Dossie> enviar(@RequestBody Dossie dossie) {
		if (erro) {
			System.out.println("Envio de email configurado para erro");
			return ResponseEntity.internalServerError().build();
		}
		emailService.enviarEmailDossie(dossie);
		System.out.println("Enviou email a partir do controller");
        return ResponseEntity.ok().build();
    }
	
	@GetMapping
	public ResponseEntity<String> alterar(@RequestParam boolean alterarErro) {
		erro = alterarErro;
		return ResponseEntity.ok("erro=" + erro);
	}
}
