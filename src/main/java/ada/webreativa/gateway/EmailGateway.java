package ada.webreativa.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import ada.webreativa.client.EmailClient;
import ada.webreativa.domain.entity.Dossie;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Component
public class EmailGateway {

    @Autowired
    EmailClient emailClient;

   @CircuitBreaker(name = "enviarEmail", fallbackMethod = "emailFallback")
    public void enviarEmail(Dossie dossie) {
	   System.out.println("Vai enviar dossie para emailClient");
   		emailClient.enviarEmail(dossie);
 	   System.out.println("Enviou dossie para emailClient");
    }

    private void emailFallback(Dossie dossie, Throwable e) {
       System.out.println("Não foi possível enviar o email");
    }


}
