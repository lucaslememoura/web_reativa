package ada.webreativa.domain.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ada.webreativa.enumeration.DossieStatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Dossie {
	private String idDossie;
	private String idCliente;
	private String nomeCliente;
	private String emailCliente;
	private DossieStatusEnum status = DossieStatusEnum.ABERTO;
	
	private List<Minuta> minutas = new ArrayList<>();



	public Dossie(String idCliente, String nomeCliente, String emailCliente) {
		super();
		this.idCliente = idCliente;
		this.nomeCliente = nomeCliente;
		this.emailCliente = emailCliente;
		this.idDossie = UUID.randomUUID().toString();
	}
	
	
	
	public void addMinuta(Minuta minuta) {
		minutas.add(minuta);
	}
	
	public void fechar() {
		this.status = DossieStatusEnum.FECHADO;
	}
}
