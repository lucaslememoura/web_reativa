package ada.webreativa.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Minuta {
	private String idInterno;
	private String idExterno;
	private String nome;
}
