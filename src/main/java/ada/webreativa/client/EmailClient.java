package ada.webreativa.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import ada.webreativa.domain.entity.Dossie;

@FeignClient(name="emailClient", url="http://localhost:8080")
public interface EmailClient {

    @PostMapping("/email")
    void enviarEmail(Dossie dossie);
}
