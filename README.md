# web_reativa

## Definição

**Criação e fechamento de dossies para envio de documentação para usuarios.**

**Serviço para criação de Minutas.**

**Serviço para envio do dossie por email (Messageria).**

## Vantagem Programação Reativa

**Foi utilizada a programação reativa nos componentes de messageria e dossie, com isso ganhamos flexibilidade na api pois processos longos e demorados não estão travando o fluxo de criação fechamento e envio de documentação.**

**Circuit Breaker: foi implementado no envio de email, para ser possivel tratar a queda do mesmo, sem comprometer outros serviços.**

## Grupo
* Adriano Souza
* Danilo Batista
* Lucas Leme de Moura
* Mauricio Cortez
* Paulo Belfi




### Rota

[http://localhost:8080/dossie](http://localhost:8080/dossie)

### POST
`{
    "idCliente":"100",
    "nomeCliente":"Paulo",
    "emailCliente":"paulobelfi@hotmail.com"
}`

### Response
`
{
"idDossie": "9a3dc6c8-a4fb-46e0-abd7-177d2c4c2d24",
"idCliente": "102",
"nomeCliente": "Lucas",
"emailCliente": "lucaslememoura@hotmail.com",
"status": "FECHADO",
"minutas": [
{
"idInterno": "f59f7862-eaef-402f-8770-4801abf90ca8",
"idExterno": "5dc4b61b-f331-4a26-8264-54baa0b7e682",
"nome": "Minuta 1"
},
{
"idInterno": "6ddf6aa3-c519-48e4-b5da-b2b07ed61fa8",
"idExterno": "df51970f-84ed-4578-820b-d8440868cf93",
"nome": "Minuta 2"
}
]
}
`
